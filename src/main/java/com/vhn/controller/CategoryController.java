package com.vhn.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.vhn.model.Category;
import com.vhn.model.DTO.CategoryDTO;
import com.vhn.model.DTO.TreeCategoryDTO;
import com.vhn.model.DataResponse;
import com.vhn.model.DataTotalResponse;
import com.vhn.service.CategoryRedisService;
import com.vhn.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/category")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private CategoryRedisService categoryRedisService;

    @PostMapping("/add-root-category")
    public ResponseEntity<DataResponse<CategoryDTO>> addRootCategory( @Valid @RequestBody CategoryDTO categoryDTO){
        categoryService.addRootCategory(categoryDTO);
        return new ResponseEntity<>(new DataResponse<>(categoryDTO), HttpStatus.OK);
    }

    @PostMapping("/add-child-category")
    public ResponseEntity<DataResponse<CategoryDTO>> addChildCategory( @Valid @RequestBody CategoryDTO categoryDTO){
        categoryService.addChildCategory(categoryDTO);
        return new ResponseEntity<>(new DataResponse<>(categoryDTO), HttpStatus.OK);
    }

    @GetMapping("/get-all")
    public ResponseEntity<DataResponse<List<Category>>> getAllCategory() throws JsonProcessingException {
        List<Category> categoriesRedis = categoryRedisService.getAllCategories();
        if (categoriesRedis.isEmpty()){
            List<Category> categories = categoryService.getAll();
            categoryRedisService.saveAllCategories(categories);
            return new ResponseEntity<>(new DataResponse<>(categories), HttpStatus.OK);
        }else {
            return new ResponseEntity<>(new DataResponse<>(categoriesRedis), HttpStatus.OK);
        }

    }

    @GetMapping("/get-tree-all")
    public ResponseEntity<DataResponse<List<TreeCategoryDTO>>> getTreeAllCategory(){
        return new ResponseEntity<>(new DataResponse<>(categoryService.getTreeCate()), HttpStatus.OK);
    }

    @GetMapping("/get-category/{id}")
    public ResponseEntity<DataResponse<Category>> getCategory(@PathVariable long id){
        return new ResponseEntity<>(new DataResponse<>(categoryService.getCategoryById(id)), HttpStatus.OK);
    }

    @GetMapping("/get-tree-category/{id}")
    public ResponseEntity<DataResponse<List<TreeCategoryDTO>>> getTreeCategory(@PathVariable long id){
        return new ResponseEntity<>(new DataResponse<>(categoryService.getTreeCate(id)), HttpStatus.OK);
    }


}
