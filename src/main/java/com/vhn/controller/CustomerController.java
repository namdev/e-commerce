package com.vhn.controller;

import com.vhn.model.Customer;
import com.vhn.model.DTO.CustomerDTO;
import com.vhn.model.DataResponse;
import com.vhn.model.StatusResponse;
import com.vhn.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/customer")
public class CustomerController {
    @Autowired
    private CustomerService customerService;

    @GetMapping("/get-all")
    public ResponseEntity<DataResponse<List<Customer>>> getAll(){
        List<Customer> customerList = customerService.getAll();
        DataResponse<List<Customer>> data = new DataResponse<>(customerList);
        return new ResponseEntity<>(data, HttpStatus.OK);
    }

    @GetMapping("/get-customer/{id}")
    public ResponseEntity<DataResponse<Customer>> getProfileById(@PathVariable long id){
        Customer customer = customerService.getCustomer(id);
        return new ResponseEntity<>(new DataResponse<>(customer), HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<StatusResponse> deleteCustomer(@PathVariable long id){
        customerService.deleteCustomer(id);
        return new ResponseEntity<>(new StatusResponse(true,"Xóa thành công"), HttpStatus.OK);
    }
}
