package com.vhn.controller;

import com.vhn.model.DTO.OrderDTO;
import com.vhn.model.DataResponse;
import com.vhn.repository.OrderDAO;
import com.vhn.service.CartItemOrderService;
import com.vhn.service.CartItemOrderServiceImpl;
import com.vhn.service.CartItemService;
import com.vhn.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/order")
public class OrderController {
    @Autowired
    private OrderService orderService;

    @PostMapping("/add-order")
    public ResponseEntity<DataResponse<Boolean>> addOrder(@RequestBody OrderDTO orderDTO){
        orderService.addOrder(orderDTO);
        return new ResponseEntity<>(new DataResponse<>(true), HttpStatus.CREATED);
    }
}
