package com.vhn.controller;

import com.vhn.model.DTO.CustomerDTO;
import com.vhn.model.DTO.LoginDTO;
import com.vhn.model.DataResponse;
import com.vhn.model.StatusResponse;
import com.vhn.service.CustomerService;
import com.vhn.service.LoginLogoutService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
public class LoginController {
    @Autowired
    private CustomerService customerService;

    @Autowired
    private LoginLogoutService loginService;


    //Đăng ký khách hàng
    @PostMapping(value = "/register/customer", consumes = "application/json")
    public ResponseEntity<DataResponse<CustomerDTO>> registerAccountCustomer(@Valid @RequestBody CustomerDTO customer) {
        DataResponse<CustomerDTO> dataResponse = new DataResponse<>();
        if (customerService.addCustomer(customer)) {
            dataResponse.setData(customer);
        }
        return new ResponseEntity<>(dataResponse, HttpStatus.CREATED);
    }

    @PostMapping(value = "/login/customer", consumes = "application/json")
    public ResponseEntity<StatusResponse> loginCustomer(@Valid @RequestBody LoginDTO loginDTO){
        return new ResponseEntity<>(loginService.loginCustomer(loginDTO), HttpStatus.OK);
    }


}
