package com.vhn.controller;

import com.vhn.model.DTO.CartItemDTO;
import com.vhn.model.DataResponse;
import com.vhn.service.CartItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/cart")
public class CartItemController {

    @Autowired
    private CartItemService cartItemService;

    @PostMapping("/add-item")
    public ResponseEntity<DataResponse<CartItemDTO>> addCartItem(@RequestBody CartItemDTO cartItemDTO){
        cartItemService.addCartItem(cartItemDTO);
        return new ResponseEntity<>(new DataResponse<>(cartItemDTO), HttpStatus.OK);
    }

    @GetMapping("/clear-item/{customerId}")
    public ResponseEntity<DataResponse<Boolean>> clearCart(@PathVariable long customerId){
        cartItemService.clearCart(customerId);
        return new ResponseEntity<>(new DataResponse<>(true), HttpStatus.OK);
    }
}
