package com.vhn.controller;

import com.vhn.model.DTO.CategoryDTO;
import com.vhn.model.DTO.ProductDTO;
import com.vhn.model.DataResponse;
import com.vhn.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/product")
public class ProductController {

    @Autowired
    private ProductService productService;

    @PostMapping("/add-product")
    public ResponseEntity<DataResponse<ProductDTO>> addProduct(@RequestBody ProductDTO productDTO){
        productService.addProduct(productDTO);
        return new ResponseEntity<>(new DataResponse<>(productDTO), HttpStatus.OK);
    }

    @PostMapping("/update-product/{id}")
    public ResponseEntity<DataResponse<ProductDTO>> updateProduct(@PathVariable long id, @RequestBody ProductDTO productDTO){
        productService.updateProduct(id,productDTO);
        return new ResponseEntity<>(new DataResponse<>(productDTO), HttpStatus.OK);
    }
}
