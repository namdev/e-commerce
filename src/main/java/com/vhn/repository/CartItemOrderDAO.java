package com.vhn.repository;

import com.vhn.model.CartItemOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CartItemOrderDAO extends JpaRepository<CartItemOrder, Long> {
}
