package com.vhn.repository;

import com.vhn.model.CartItem;
import com.vhn.model.Customer;
import com.vhn.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CartItemDAO extends JpaRepository<CartItem, Long> {
    Optional<CartItem> findCartItemByCartProductAndCartCustomer(Product cartProduct, Customer cartCustomer);

    List<CartItem> findCartItemsByCartCustomer(Customer customer);
}
