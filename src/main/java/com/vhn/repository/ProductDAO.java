package com.vhn.repository;

import com.vhn.model.CartItem;
import com.vhn.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProductDAO extends JpaRepository<Product, Long> {
    Optional<Product> findProductByProductId(long id);

}
