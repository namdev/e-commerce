package com.vhn.repository;

import com.vhn.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CategoryDAO extends JpaRepository<Category, Long> {
    Optional<Category> findCategoryByCateName(String name);
    Optional<Category> findCategoryByCateId(long id);

    List<Category> findAllByActiveIsNotNull();
}
