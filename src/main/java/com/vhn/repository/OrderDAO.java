package com.vhn.repository;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.vhn.model.Customer;
import com.vhn.model.Order;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderDAO extends JpaRepository<Order, Long> {

}
