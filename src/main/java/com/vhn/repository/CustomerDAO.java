package com.vhn.repository;

import com.vhn.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CustomerDAO extends JpaRepository<Customer, Long> {
    Optional<Customer> findByMobileNo(String mobileNo);
    Optional<Customer> findCustomerByCustomerId(long id);
    List<Customer> findAll();
}
