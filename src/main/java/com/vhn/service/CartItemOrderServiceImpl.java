package com.vhn.service;

import com.vhn.model.CartItemOrder;
import com.vhn.repository.CartItemOrderDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CartItemOrderServiceImpl implements CartItemOrderService {
    @Autowired
    private CartItemOrderDAO cartItemOrderDAO;
    @Override
    public void addCartItemOrder(CartItemOrder cartItemOrder) {
        cartItemOrderDAO.save(cartItemOrder);
    }
}
