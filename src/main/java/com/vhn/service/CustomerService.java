package com.vhn.service;

import com.vhn.model.Customer;
import com.vhn.model.DTO.CustomerDTO;

import java.util.List;

public interface CustomerService {
    boolean addCustomer(CustomerDTO customerDto);
    List<Customer> getAll();
    void deleteCustomer(long id);
    Customer getCustomer(long id);

}
