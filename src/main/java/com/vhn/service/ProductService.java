package com.vhn.service;

import com.vhn.model.CartItem;
import com.vhn.model.DTO.ProductDTO;
import com.vhn.model.Product;
import org.springframework.stereotype.Service;

import java.util.List;


public interface ProductService {
    void addProduct(ProductDTO productDTO);
    void updateProduct(long id, ProductDTO productDTO);
    Product getProduct(long id);
    boolean checkAvailableProduct(Product product);
    void updateQuantityProduct(long id, int quantity );
}
