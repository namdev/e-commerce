package com.vhn.service;

import com.vhn.exception.CartException;
import com.vhn.model.*;
import com.vhn.model.DTO.OrderDTO;
import com.vhn.model.enums.OrderStatusEnum;
import com.vhn.repository.CustomerDAO;
import com.vhn.repository.OrderDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderDAO orderDAO;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private ProductService productService;

    @Autowired
    private CartItemService cartItemService;

    @Autowired
    private CartItemOrderService cartItemOrderService;
    @Override
    public void addOrder(OrderDTO orderDTO) {
        Customer customer = customerService.getCustomer(orderDTO.getCustomerId());
        List<CartItem> cartItemList = cartItemService.getAllItem(orderDTO.getCustomerId());
        if (cartItemList.isEmpty()){
            throw new CartException("Giỏ hàng rỗng");
        }
        Order order = new Order();
        order.setCustomerOrder(customer);
        order.setOrderStatus(OrderStatusEnum.SUCCESS);
        order.setCreatedAt(LocalDate.now());
        Order orderSaved = orderDAO.saveAndFlush(order);
        final long[] totalPriceOrder = {0};
        cartItemList.forEach(c -> {
            CartItemOrder cartItemOrder = new CartItemOrder();
            cartItemOrder.setOrderId(orderSaved.getOrderId());
            cartItemOrder.setCartItemId(c.getCartItemId());
            cartItemOrder.setProductName(c.getCartProduct().getProductName());
            if (c.getQuantity() < c.getCartProduct().getQuantity()){
                cartItemOrder.setQuantity(c.getQuantity());
                long totalPrice = (long) (c.getCartProduct().getPrice() * c.getQuantity());
                totalPriceOrder[0] += totalPrice;
                cartItemOrder.setTotalPrice(totalPrice);
                productService.updateQuantityProduct(c.getCartProduct().getProductId(),c.getCartProduct().getQuantity()- c.getQuantity());
            }else {
                cartItemOrder.setQuantity(c.getCartProduct().getQuantity());
                long totalPrice = (long) (c.getCartProduct().getPrice() * c.getCartProduct().getQuantity());
                totalPriceOrder[0] += totalPrice;
                cartItemOrder.setTotalPrice(totalPrice);
                productService.updateQuantityProduct(c.getCartProduct().getProductId(),0);
            }
            cartItemOrderService.addCartItemOrder(cartItemOrder);
        });

        orderSaved.setTotalPrice(totalPriceOrder[0]);
        orderSaved.setTotal((int) cartItemList.stream().count());
        orderSaved.setAddress(orderDTO.getAddress());
        orderDAO.save(orderSaved);


    }
}
