package com.vhn.service;

import com.vhn.model.Category;
import com.vhn.model.DTO.CategoryDTO;
import com.vhn.model.DTO.TreeCategoryDTO;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

public interface CategoryService {
    void addRootCategory(CategoryDTO categoryDTO);
    void addChildCategory(CategoryDTO categoryDTO);
    List<Category> getAll();
    List<TreeCategoryDTO> getTreeCate();
    List<TreeCategoryDTO> getTreeCate(long id);
    Category getCategoryById(long id);
}
