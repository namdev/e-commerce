package com.vhn.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vhn.config.RedisConfig;
import com.vhn.model.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CategoryRedisServiceImpl implements CategoryRedisService{

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    @Autowired
    private ObjectMapper redisObjectMapper;

    @Override
    public void clear() {

    }

    @Override
    public List<Category> getAllCategories() throws JsonProcessingException {
        String key = getKey();
        String json = (String) redisTemplate.opsForValue().get(key);
        List<Category> categories = json !=null ?
                redisObjectMapper.readValue(json, new TypeReference<List<Category>>() {}) : new ArrayList<>();
        return categories;
    }

    @Override
    public void saveAllCategories(List<Category> categories) throws JsonProcessingException {
        String key = getKey();
        String json = redisObjectMapper.writeValueAsString(categories);
        redisTemplate.opsForValue().set(key,json);
    }

    private String getKey(){
        return "all_categories";
    }
}
