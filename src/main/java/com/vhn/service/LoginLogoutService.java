package com.vhn.service;

import com.vhn.model.DTO.LoginDTO;
import com.vhn.model.StatusResponse;

public interface LoginLogoutService {
    StatusResponse loginCustomer(LoginDTO loginCustomer);
    StatusResponse logout();
}
