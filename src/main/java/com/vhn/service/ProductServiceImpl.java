package com.vhn.service;

import com.vhn.exception.CartException;
import com.vhn.exception.ProductNotFoundException;
import com.vhn.model.CartItem;
import com.vhn.model.DTO.ProductDTO;
import com.vhn.model.Product;
import com.vhn.model.enums.ProductStatusEnum;
import com.vhn.repository.ProductDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductDAO productDAO;

    @Autowired
    private CategoryService categoryService;

    @Override
    public void addProduct(ProductDTO productDTO) {
        Product product = new Product();
        transferProductDto(product,productDTO);
        product.setCreatedAt(LocalDate.now());
        productDAO.save(product);
    }

    @Override
    public void updateProduct(long id,ProductDTO productDTO) {
        Product product = getProduct(id);
        transferProductDto(product,productDTO);
        productDAO.save(product);
    }

    @Override
    public Product getProduct(long id) {
        Optional<Product> product = productDAO.findProductByProductId(id);
        if (product.isEmpty()){
            throw new ProductNotFoundException("Không tìm thấy sản phẩm");
        }
        return product.get();
    }

    @Override
    public boolean checkAvailableProduct(Product product) {
        if (product.getProductStatus() == ProductStatusEnum.OUTOFSTOCK || product.getQuantity() == 0){
            return false;
        }
        return true;
    }

    @Override
    public void updateQuantityProduct(long id, int quantity) {
        Product product = getProduct(id);
        product.setQuantity(quantity);
        productDAO.save(product);
    }




    public void transferProductDto(Product product, ProductDTO productDTO){
        product.setProductName(productDTO.getProductName());
        product.setProductStatus(ProductStatusEnum.AVAILABLE);
        product.setActive(true);
        product.setPrice(productDTO.getPrice());
        product.setDescription(productDTO.getDescription());
        product.setQuantity(productDTO.getQuantity());
        product.setBought(0);
        product.setUpdatedAt(LocalDate.now());
        product.setCategoryProduct(categoryService.getCategoryById(productDTO.getCategoryId()));
    }
}
