package com.vhn.service;


import com.vhn.model.CartItem;
import com.vhn.model.Customer;
import com.vhn.model.DTO.CartItemDTO;
import com.vhn.model.Product;

import java.util.List;

public interface CartItemService {
    void addCartItem(CartItemDTO cartItemDTO);

    void clearCart(long customerId);
    List<CartItem> getAllItem(long customerId);
}
