package com.vhn.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.vhn.model.Category;

import java.util.List;

public interface CategoryRedisService {
    void clear();
    List<Category> getAllCategories() throws JsonProcessingException;
    void saveAllCategories(List<Category> categories) throws JsonProcessingException;
}
