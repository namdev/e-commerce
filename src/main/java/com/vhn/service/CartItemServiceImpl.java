package com.vhn.service;

import com.vhn.exception.CartException;
import com.vhn.model.CartItem;
import com.vhn.model.Customer;
import com.vhn.model.DTO.CartItemDTO;
import com.vhn.model.Product;
import com.vhn.model.enums.ProductStatusEnum;
import com.vhn.repository.CartItemDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CartItemServiceImpl implements CartItemService{

    @Autowired
    private CartItemDAO cartItemDAO;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private ProductService productService;
    @Override
    public void addCartItem(CartItemDTO cartItemDTO) {
        Product product = productService.getProduct(cartItemDTO.getProductId());
        Customer customer = customerService.getCustomer(cartItemDTO.getCustomerId());
        if (!productService.checkAvailableProduct(product)){
            throw new CartException("Đã hết hàng");
        }
        CartItem cartItem = productExistInCart(product,customer);
        if (cartItem.getCartItemId()==0) {
            cartItem.setCartProduct(product);
            cartItem.setCartCustomer(customer);
        }
        if (product.getQuantity() >= cartItemDTO.getQuantity()) {
            cartItem.setQuantity(cartItemDTO.getQuantity());
        } else {
            cartItem.setQuantity(product.getQuantity());
        }
        cartItem.setActive(true);
        cartItemDAO.save(cartItem);

    }

    @Override
    public void clearCart(long customerId) {
        Customer customer = customerService.getCustomer(customerId);
        List<CartItem> cartItemList = cartItemDAO.findCartItemsByCartCustomer(customer);
        cartItemList.stream().forEach(c -> {
            c.setQuantity(0);
            c.setActive(false);
        });
        cartItemDAO.saveAll(cartItemList);
    }

    @Override
    public List<CartItem> getAllItem(long customerId){
        Customer customer = customerService.getCustomer(customerId);
        List<CartItem> cartItemList = cartItemDAO.findCartItemsByCartCustomer(customer);
        cartItemList = cartItemList.stream().filter(c -> c.getQuantity()>0 && c.isActive()).toList();
        return cartItemList;
    }

    public CartItem productExistInCart(Product product, Customer customer) {
        Optional<CartItem> cartItem = cartItemDAO.findCartItemByCartProductAndCartCustomer(product, customer);
        return cartItem.orElse(new CartItem());
    }
}
