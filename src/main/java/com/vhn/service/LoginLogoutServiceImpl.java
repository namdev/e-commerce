package com.vhn.service;

import com.vhn.model.*;
import com.vhn.model.DTO.LoginDTO;
import com.vhn.repository.CustomerDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class LoginLogoutServiceImpl implements LoginLogoutService{


    @Autowired
    private CustomerDAO customerDao;

    @Override
    public StatusResponse loginCustomer(LoginDTO loginCustomer) {
        Optional<Customer> res = customerDao.findByMobileNo(loginCustomer.getMobileNo());
        if(res.isEmpty())
            return new StatusResponse(false,"Số điện thoại không tồn tại!");
        Customer existCustomer = res.get();

        if (existCustomer.getPassword().equals(loginCustomer.getPassword())){
            return new StatusResponse(true,"Đăng nhập thành công");
        }else{
            return new StatusResponse(false,"Thông tin đăng nhập không đúng");
        }
    }

    @Override
    public StatusResponse logout() {
        return new StatusResponse(true,"");
    }


}
