package com.vhn.service;

import com.vhn.model.CartItemOrder;

public interface CartItemOrderService {
    void addCartItemOrder(CartItemOrder cartItemOrder);
}
