package com.vhn.service;

import com.vhn.model.DTO.OrderDTO;

public interface OrderService {
    void addOrder(OrderDTO orderDTO);
}
