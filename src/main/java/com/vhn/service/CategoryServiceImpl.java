package com.vhn.service;

import com.vhn.exception.CategoryException;
import com.vhn.model.Category;
import com.vhn.model.DTO.CategoryDTO;
import com.vhn.model.DTO.TreeCategoryDTO;
import com.vhn.repository.CategoryDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class CategoryServiceImpl implements CategoryService {
    @Autowired
    private CategoryDAO categoryDAO;

    @Override
    public void addRootCategory(CategoryDTO categoryDTO) {
        checkExistCategory(categoryDTO.getCateName());
        Category category = new Category();
        category.setCateName(categoryDTO.getCateName());
        category.setCateLevel(1);
        category.setCateParent(0);
        categoryDAO.save(category);
    }

    @Override
    public void addChildCategory(CategoryDTO categoryDTO) {
        checkExistCategory(categoryDTO.getCateName());
        Category category = new Category();
        category.setCateName(categoryDTO.getCateName());
        category.setCateParent(categoryDTO.getCateParent());
        category.setCateLevel(getLevel(categoryDTO.getCateParent()));
        categoryDAO.save(category);
    }

    @Override
    public List<Category> getAll() {
        return categoryDAO.findAllByActiveIsNotNull();
    }

    @Override
    public List<TreeCategoryDTO> getTreeCate() {
        List<Category> allCate = categoryDAO.findAllByActiveIsNotNull();
        List<Category> cateLevel1 = allCate.stream().filter(x -> x.getCateLevel() == 1).toList();
        List<TreeCategoryDTO> dataResult = new ArrayList<>();
        for (Category cate : cateLevel1) {
            TreeCategoryDTO treeCategory = new TreeCategoryDTO();
            treeCategory.setCategory(cate);
            List<TreeCategoryDTO> childCates = GetCate(cate, allCate);
            if (!childCates.isEmpty()) {
                treeCategory.setChildCategory(childCates);
            }
            dataResult.add(treeCategory);
        }
        return dataResult;
    }

    @Override
    public List<TreeCategoryDTO> getTreeCate(long id) {
        List<Category> allCate = categoryDAO.findAllByActiveIsNotNull();
        Optional<Category> cateLevel1 = categoryDAO.findCategoryByCateId(id);
        if (cateLevel1.isEmpty())
            throw new CategoryException("Không tồn tại danh mục này");
        List<TreeCategoryDTO> dataResult = new ArrayList<>();

        TreeCategoryDTO treeCategory = new TreeCategoryDTO();
        treeCategory.setCategory(cateLevel1.get());
        List<TreeCategoryDTO> childCates = GetCate(cateLevel1.get(), allCate);
        if (!childCates.isEmpty()) {
            treeCategory.setChildCategory(childCates);
        }
        dataResult.add(treeCategory);

        return dataResult;
    }

    @Override
    public Category getCategoryById(long id) {
        Optional<Category> categoryOp = categoryDAO.findCategoryByCateId(id);
        if (categoryOp.isEmpty())
            throw new CategoryException("Không tồn tại danh mục này");
        return categoryOp.get();
    }

    private List<TreeCategoryDTO> GetCate(Category cate, List<Category> allCate) {
        List<Category> childCates = allCate.stream().filter(x -> x.getCateParent() == cate.getCateId()).toList();
        List<TreeCategoryDTO> treeChildCategoryList = new ArrayList<>();
        if (!childCates.isEmpty()) {
            for (Category childCate : childCates) {
                TreeCategoryDTO treeChildCategory = new TreeCategoryDTO();
                treeChildCategory.setCategory(childCate);
                treeChildCategory.setChildCategory(GetCate(childCate, allCate));
                treeChildCategoryList.add(treeChildCategory);
            }
        }

        return treeChildCategoryList;
    }

    public void checkExistCategory(String name) {
        Optional<Category> categoryOp = categoryDAO.findCategoryByCateName(name);
        if (categoryOp.isPresent())
            throw new CategoryException("Tên danh mục này đã tồn tại");
    }

    public int getLevel(int cateParent) {
        Optional<Category> categoryOp = categoryDAO.findCategoryByCateId(cateParent);
        if (categoryOp.isEmpty())
            throw new CategoryException("Lỗi");
        return categoryOp.get().getCateLevel() + 1;
    }
}
