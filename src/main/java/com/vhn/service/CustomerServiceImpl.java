package com.vhn.service;

import com.vhn.exception.CustomerException;
import com.vhn.model.Customer;
import com.vhn.model.DTO.CustomerDTO;
import com.vhn.repository.CustomerDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CustomerServiceImpl implements CustomerService{

    @Autowired
    private CustomerDAO customerDAO;


    @Override
    public boolean addCustomer(CustomerDTO customerDto) {
        Optional<Customer> existCustomer = customerDAO.findByMobileNo(customerDto.getMobileNo());
        if (existCustomer.isPresent())
            throw new CustomerException("Số điện thoại đăng ký đã tồn tại");
        Customer newCustomer = new Customer();
        newCustomer.setFirstName(customerDto.getFirstName());
        newCustomer.setLastName(customerDto.getLastName());
        newCustomer.setMobileNo(customerDto.getMobileNo());
        newCustomer.setEmail(customerDto.getEmail());
        newCustomer.setPassword(customerDto.getPassword());
        customerDAO.save(newCustomer);
        return true;
    }

    @Override
    public List<Customer> getAll(){
        return customerDAO.findAll();
    }

    @Override
    public void deleteCustomer(long id) {
        Optional<Customer> customer = customerDAO.findCustomerByCustomerId(id);
        if (customer.isEmpty())
            throw new CustomerException("Tài khoản không tồn tại");
        customerDAO.delete(customer.get());
    }

    @Override
    public Customer getCustomer(long id) {
        Optional<Customer> customer = customerDAO.findCustomerByCustomerId(id);
        if (customer.isEmpty())
            throw new CustomerException("Tài khoản không tồn tại");

        return customer.get();
    }


}
