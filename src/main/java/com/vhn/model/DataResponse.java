package com.vhn.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DataResponse<D>{
    private D data;
    private boolean status = true ;
    private String message;

    public DataResponse(boolean status, D data) {
        this.status = status;
        this.data = data;
    }

    public DataResponse(D data) {
        this.data = data;
    }
}
