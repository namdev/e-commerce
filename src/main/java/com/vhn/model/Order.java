package com.vhn.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.vhn.model.enums.OrderStatusEnum;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import java.time.LocalDate;
import java.util.Collection;


@AllArgsConstructor
@NoArgsConstructor
@Data
@ToString
@Entity(name = "orderProduct")
public class Order {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private long orderId;
    @PastOrPresent
    private LocalDate createdAt;
    @NotNull
    @Enumerated(EnumType.STRING)
    private OrderStatusEnum orderStatus;

    private int total;
    private long totalPrice;

    private String address;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "customerId", referencedColumnName = "customerId")
    private Customer customerOrder;


}
