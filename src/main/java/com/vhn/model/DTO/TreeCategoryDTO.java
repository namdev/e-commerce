package com.vhn.model.DTO;

import com.vhn.model.Category;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TreeCategoryDTO {
    private Category category;

    private List<TreeCategoryDTO> childCategory;
}
