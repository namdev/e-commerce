package com.vhn.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.NaturalIdCache;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DataTotalResponse<D> extends DataResponse<D>{
    private long total;

    public DataTotalResponse(long total, D data) {
        super(data);
        this.total = total;
    }
}
