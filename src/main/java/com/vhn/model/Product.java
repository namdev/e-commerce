package com.vhn.model;

import com.vhn.model.enums.ProductStatusEnum;
import jakarta.persistence.*;
import lombok.*;
import org.antlr.v4.runtime.misc.NotNull;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity(name = "product")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long productId;

    @NotNull
    @Size(min = 3, max = 30, message = "Product name size should be between 3-30")
    private String productName;

    @NotNull
    @DecimalMin(value = "0.00")
    private Double price;

    private String description;

    @NotNull
    private boolean active;

    @NotNull
    @Min(value = 0)
    private int quantity;

    @NotNull
    @Min(value = 0)
    private int bought;

    private LocalDate createdAt;
    private LocalDate updatedAt;

    @ManyToOne
    @JoinColumn(name = "categoryId")
    @EqualsAndHashCode.Exclude
    private Category categoryProduct;

    @Enumerated(EnumType.STRING)
    private ProductStatusEnum productStatus;

    @OneToMany(mappedBy = "cartProduct")
    @EqualsAndHashCode.Exclude
    private List<CartItem> cartItemList;

}
