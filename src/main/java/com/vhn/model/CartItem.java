package com.vhn.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.antlr.v4.runtime.misc.NotNull;

import java.util.Collection;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity(name = "cartItem")
public class CartItem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long cartItemId;

    @ManyToOne
    @JoinColumn(name = "productId")
    @JsonIgnoreProperties(value={
            "productId",
            "seller",
            "quantity"

    })
    private Product cartProduct;

    @ManyToOne
    @JoinColumn(name = "customerId", nullable = false)
    @JsonIgnore
    private Customer cartCustomer;

    private int quantity;

    private boolean active;
}
