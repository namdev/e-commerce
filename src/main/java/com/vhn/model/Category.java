package com.vhn.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.vhn.model.DTO.CategoryDTO;
import jakarta.persistence.*;
import lombok.*;
import org.springframework.beans.factory.annotation.Value;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity(name = "category")
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long cateId;

    @NotNull
    private String cateName;

    @NotNull
    private int cateLevel;

    @NotNull
    private int cateParent;

    private boolean active = true;

    @OneToMany(mappedBy = "categoryProduct")
    @JsonIgnore
    @EqualsAndHashCode.Exclude
    private List<Product> productList;
}
