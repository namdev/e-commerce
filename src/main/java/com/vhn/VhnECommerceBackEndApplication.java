package com.vhn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VhnECommerceBackEndApplication {

    public static void main(String[] args) {
        SpringApplication.run(VhnECommerceBackEndApplication.class, args);
    }

}
