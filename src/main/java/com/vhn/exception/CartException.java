package com.vhn.exception;

public class CartException extends RuntimeException{
    public CartException(String message) {
        super(message);
    }
}
