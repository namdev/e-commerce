package com.vhn.exception;

public class CategoryException extends RuntimeException{
    public CategoryException(String message) {
        super(message);
    }
}
